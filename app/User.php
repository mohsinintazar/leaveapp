<?php

namespace Leave;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Creating a public function to get the user Department from Department table through department_id.
     * @return eloquent relation
     */
    public function department(){
        return $this->belongsTo('Leave\Department');
    }
    
    /**
     * Creating a public function to get the user Role from Role table through role_id.
     * @return eloquent relation
     */    
    public function role(){
        return $this->belongsTo('Leave\Role');
    }    
}
