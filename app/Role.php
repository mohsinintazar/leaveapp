<?php

namespace Leave;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Creating a public function to get the user from Users table through role_id in the users table.
     * @return eloquent relation
     */        
    public function user(){
        return $this->hasMany('Leave\User');
    }    
}
