@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>
                <div class="panel-body">
                    <h1>KKT Pakistan Online HR System</h1>
                    <h2>Rules applicable</h2>
                    <ul>
                        <li>If you are new to this system then, click on the top right corner button to 
                            register yourself.</li>
                        <li>If you are already a part of KKT Family then login to the application to view
                        your leave history or apply for a new leave.</li>
                        <li>The leave you are applying for will be directly emailed to your relevant supervisor.</li>
                        <li>Once your supervisor accepts or rejects your leave application, an email notification
                        will be sent to the HR department and to your provided email address.</li>
                        <li>Your default leave application status will always be "Pending" state and it will 
                            be updated only when your supervisor responds back to it.</li>
                        <li>All of the leaves are renewed according to the calendar year.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
